Name:           udisks-qt5
Version:        0.0
Release:        1%{?dist}
Summary:        A library for talking with UDisks2 with a Qt like API

License:        GPLv2+
URL:            https://gitorious.org/udisks-qt
Source0:        %{name}-%{version}.tar.xz

BuildRequires:  qt5-qtbase-devel

%description
udisks-qt is a library for talking with UDisks2 with a Qt like API

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%setup -q


%build
mkdir build && cd build
%cmake ..
make %{?_smp_mflags}


%install
cd build
%make_install
find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';'


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%doc
%{_libdir}/*.so.*

%files devel
%doc
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/cmake/*
%{_libdir}/pkgconfig/*


%changelog
* Sat Aug  2 2014 Hedayat Vatankhah <hedayat.fwd+rpmchlog@gmail.com>
- Initial version
